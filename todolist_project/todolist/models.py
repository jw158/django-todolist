from django.db import models

# Create your models here.


class TodoCategory(models.Model):
    name = models.CharField(max_length=32, blank=True, null=True)

    def __str__(self):
        return f"{self.name}"


class TodoItem(models.Model):
    status_choices = [("A", "Active"), ("D", "Done"), ("C", "Canceled")]

    title = models.CharField(max_length=32, blank=False)
    description = models.TextField()
    # pub_date = models.DateTimeField(blank=True, null=True)  # For later: Somehow get a current time in format of DateTimeField
    # due_date = models.DateTimeField(blank=True, null=True)
    due_date = models.CharField(max_length=32, default="yyyy-mm-dd")
    due_time = models.CharField(max_length=32, default="hh:mm:ss")
    important = models.BooleanField(default=False)
    status = models.CharField(max_length=1, choices=status_choices, default="A")
    category = models.ForeignKey(TodoCategory, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.title} {self.category} {self.due_date} {self.due_time} {self.status}"




