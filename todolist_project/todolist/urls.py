from django.contrib import admin
from django.urls import path, include

from . import views

# app_name = "todolist"

urlpatterns = [
    # path("", views.index_view, name="index_view"),
    path("", views.list_view, name="list_view"),
    path("about/", views.about_view, name="about_view"),
    path("<int:item_id>/", views.list_item_view, name="list_item_view"),
    path("<int:item_id>/status", views.change_status_view, name="change_status_view"),
    path("new_item/", views.new_item_view, name="new_item_view"),
]
