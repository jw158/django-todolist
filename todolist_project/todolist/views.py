from .models import TodoItem
from .models import TodoCategory

# from datetime import datetime

from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.urls import reverse


# Create your views here.


def index_view(request):
    return render(request, "todolist/index.html")


def list_view(request):
    # item = TodoItem.objects.get(id=1)  # Funktioniert trotz Warnung, kein Django-Support für PyCharm Edu :(
    # context = {
    #     "title": item.title,
    #     "description": item.description
    # }
    item_list = TodoItem.objects.all()
    context = {
        "item_list": item_list,
    }
    # return render(request, "todolist/list.html", context)
    return render(request, 'todolist/list.html', {"item_list": item_list, "status_choices": TodoItem.status_choices, "categories": TodoCategory.objects.all()})


def list_item_view(request, item_id):
    item = TodoItem.objects.get(pk=item_id)
    return render(request, "todolist/list_item.html", {"item": item})


def change_status_view(request, item_id):
    item = TodoItem.objects.get(pk=item_id)
    item.status = request.POST["status"]  # same as select tag
    item.save()
    url = reverse("list_item_view", kwargs={"item_id": item.id})  # https://stackoverflow.com/a/10785145
    return HttpResponseRedirect(url)  # from django.http import HttpResponseRedirect, from django.urls import reverse


def new_item_view(request):
    title = request.POST["title"]
    description = request.POST["description"]
    status = request.POST["status"]
    due_date = request.POST["due_date"]
    # due_time = request.POST["due_time"]
    category = TodoCategory.objects.get(pk=request.POST["category"])

    print(title, description, status, due_date, category)

    TodoItem.objects.create(title=title, description=description, status=status, due_date=due_date, category=category) # due_time=due_time
    return HttpResponseRedirect(reverse('list_view'))


def about_view(request):
    return render(request, "todolist/about.html")
